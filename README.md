# Modelos

Modelos creados para el ramo PBD


# Supuestos

## Actividades Organizador
* Crear usuario
* Organizar evento
* Editar evento
* Cancelar evento
* Ejecutar evento

## Actividades Visitantes (persona logeada)
* Crear cuenta, para poder participar en evento
* Buscar eventos
* Participar en evento: financiando

## Actividades Visitantes (persona no logeada
* Ver todos los eventos

# Actividades

## Crear Evento

* Ingresar detalles eventos
    + Nombre
    + Meta de participantes
    + Descripcion
+ Eleccion de lugar: propio o de algun proveedor
+ Eleccion de recursos: 
    + especificando el proveedor en especifico
    + o eligiendo un recurso en general -> notificar a proveedores que ofrecen el servicio

## Editar Evento

## Eliminar Evento

## Registrar Lugar

## 



# Librerias a utilizar

## Convertir la direcci�n a coordenadas mediante google maps
http://hpneo.github.io/gmaps/examples/geocoding.html
